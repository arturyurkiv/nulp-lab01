﻿using System;
using Application;
using Lab01.Models;

namespace Lab01.View
{
    public class ResultView
    {

        public static void GetSortingResult(DataResultModel dataResultModel)
        {
            Console.WriteLine("Sorting  vase by water descent with Selections algoritm.");
            Console.WriteLine("Execution time: " + dataResultModel.SelectionAlgoritmWorkTime + "!!!");
            Console.WriteLine("Comparisons count: " + dataResultModel.SelectionAlgoritmСomparisonsCount + "!!!");
            Console.WriteLine("Exchange count: " + dataResultModel.SelectionAlgoritmExchangeCount + "!!!");

            Console.WriteLine("Result:");

            
            Console.Write("{ ");

            foreach(var currentVase in dataResultModel.InsertSortedVases)
            {
                Console.Write(currentVase + ", ");
            }
            Console.WriteLine(" }");

            
            Console.WriteLine("Sorting vase by weight by growth with merge algoritm.");
            Console.WriteLine("Execution time: " + dataResultModel.MergeAlgoritmWorkTime + "!!!");
            Console.WriteLine("Comparisons count: " + dataResultModel.MergeAlgoritmСomparisonsCount + "!!!");
            Console.WriteLine("Exchange count: " + dataResultModel.MergeAlgoritmExchangeCount + "!!!");

            Console.WriteLine("Result:");
            Console.Write("{ ");

            foreach (var currentVase in dataResultModel.MergeSortedVases)
            {
                Console.Write(currentVase + ", ");
            }
            Console.Write(" }");
            
        }



    }
}
