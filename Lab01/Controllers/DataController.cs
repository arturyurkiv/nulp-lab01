﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Application;
using Lab01.Models;

namespace Lab01.Controllers
{
    public class DataController
    {
        public static DataResultModel DataResult;


        public static void InitData()
        {
            DataResult = new DataResultModel();

            DataResult.Vases = DataController.ProcessCsv("Vases.csv");
            
            var insertResult = SortingController.SortByWaterDescent (DataResult.Vases);
            var mergeResult = SortingController.SortByWeightByGrowth(DataResult.Vases);

            
            DataResult.InsertSortedVases = insertResult.Item1;
            DataResult.SelectionAlgoritmСomparisonsCount = insertResult.Item2;
            DataResult.SelectionAlgoritmExchangeCount = insertResult.Item3;

            
            DataResult.MergeSortedVases = mergeResult.Item1;
            DataResult.MergeAlgoritmСomparisonsCount = mergeResult.Item2;
            DataResult.MergeAlgoritmExchangeCount = mergeResult.Item3;
            
        }

        /// <summary>
        /// Seserialise csv file from path
        /// </summary>
        /// <param name="path">file path.</param>
        /// <returns></returns>
        public static List<Vase> ProcessCsv(string path)
        {
            return File.ReadAllLines(path)
                //.Skip(0)
                .Where(row => row.Length > 0)
                .Select(Vase.Parse).ToList();
        }

    }
}
