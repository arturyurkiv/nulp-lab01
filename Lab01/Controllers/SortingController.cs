﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lab01.Controllers;

namespace Application
{
    public  class SortingController
    {
        private static int _mergeAlgoritmСomparisonsCount;
        private static int _mergAlgoritmExchangeCount;
        private static int _selectionAlgoritmСomparisonsCount;
        private static int _selectionAlgoritmExchangeCount;

        /// <summary>
        /// Sorting  vase by water descent with Selections algoritm.
        /// </summary>
        /// <param name="vases"></param>
        /// <returns>List<float> sortedList, int _selectionAlgoritmСomparisonsCount, int _selectionAlgoritmExchangeCount  </returns>
        public static (List<float>,int,int) SortByWaterDescent(List<Vase> vases)
		{
            var starttime = DateTime.Now;

            var result = (SelectionSort(vases.Select(x => x.WaterVolume).ToList()), _selectionAlgoritmСomparisonsCount, _selectionAlgoritmExchangeCount);
            var endtime = DateTime.Now;
            DataController.DataResult.SelectionAlgoritmWorkTime = (starttime - endtime).ToString();

            return result;
        }

        /// <summary>
		/// Sorting vase by weight by growth with merge algoritm.
		/// </summary>
        /// <param name="vases"></param>
        /// <returns>List<float> sortedList, int _selectionAlgoritmСomparisonsCount, int _selectionAlgoritmExchangeCount  </returns>
        public static (List<float>,int,int) SortByWeightByGrowth(List<Vase> vases)
		{
            var starttime = DateTime.Now;

            var result =  (MergeSort(vases.Select(x => x.VaseWeight).ToList()),_mergeAlgoritmСomparisonsCount,_mergAlgoritmExchangeCount);

            var endtime = DateTime.Now;
            DataController.DataResult.SelectionAlgoritmWorkTime = (starttime - endtime).ToString();

            return result;
        }

        private static List<float> SelectionSort(List<float> vases)
        {
            int vasesLenth = vases.Count;
            var sortedVases = vases;

            for (int i = 0; i < vasesLenth - 1; i++)
            {
                int maxIndex = i;
                for (int j = i + 1; j < vasesLenth; j++)
                {
                    if (sortedVases[j] >= sortedVases[maxIndex])
                    {
                        maxIndex = j;
                        _selectionAlgoritmExchangeCount++;
                        
                    }
                }
                var temp = sortedVases[maxIndex];
                sortedVases[maxIndex] = sortedVases[i];
                sortedVases[i] = temp;
                _selectionAlgoritmСomparisonsCount++;

            }

            return sortedVases;

        }

        private static List<float> MergeSort(List<float> unsorted)
        {
            if (unsorted.Count <= 1)
                return unsorted;

            List<float> left = new List<float>();
            List<float> right = new List<float>();

            int middle = unsorted.Count / 2;
            for (int i = 0; i < middle; i++)  
            {
                left.Add(unsorted[i]);
            }
            for (int i = middle; i < unsorted.Count; i++)
            {
                right.Add(unsorted[i]);
            }

            left = MergeSort(left);
            right = MergeSort(right);
            return Merge(left, right);
        }

        private static List<float> Merge(List<float> left, List<float> right)
        {
            List<float> result = new List<float>();

            while (left.Count > 0 || right.Count > 0)
            {
                if (left.Count > 0 && right.Count > 0)
                {

                    if (left.First() <= right.First())  
                    {
                        result.Add(left.First());
                        left.Remove(left.First());
                        _mergeAlgoritmСomparisonsCount++;
                        _mergAlgoritmExchangeCount++;

                    }
                    else
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                        _mergeAlgoritmСomparisonsCount++;
                    }
                   
                }
                else if (left.Count > 0)
                {
                    result.Add(left.First());
                    left.Remove(left.First());
                    _mergeAlgoritmСomparisonsCount++;
                    _mergAlgoritmExchangeCount++;
                }
                else if (right.Count > 0)
                {
                    result.Add(right.First());
                    right.Remove(right.First());
                    _mergeAlgoritmСomparisonsCount++;
                }
                
               
            }
            return result;
        }

        private static void Swap(ref int x, ref int y)
        {
            int temp = x;
            x = y;
            y = temp;
            _selectionAlgoritmExchangeCount++;
        }

    }
}
