﻿using System;

namespace Application
{
    public class Vase
    {
        private int _id;
        private float _waterVolume;
        private float _vaseWeight;

        /// <summary>
        /// Vase id.
        /// </summary>
        public int ID
        {
            get => _id;
            set => _id = value;
        }

        /// <summary>
        /// Water content in the vase
        /// </summary>
        public float WaterVolume
		{
			get => _waterVolume > 0 ? _waterVolume : 0f;
            set => _waterVolume = value;   
		}

        /// <summary>
        /// Vase weight
        /// </summary>
        public float VaseWeight
        {
            get => _vaseWeight >= 0 ? _vaseWeight : 0f;
            set => _vaseWeight = value;
        }

        /// <summary>
        /// Parce .csv format to Vase class.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static Vase Parse(string row)
        {
            var columns = row.Split(';');
            return new Vase()
            {
                ID = int.Parse(columns[0]),
                WaterVolume = float.Parse(columns[1]),
                VaseWeight = float.Parse(columns[2])
            };
        }


    }
}
