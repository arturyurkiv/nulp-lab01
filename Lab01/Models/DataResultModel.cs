﻿using System;
using System.Collections.Generic;
using Application;

namespace Lab01.Models
{
    public class DataResultModel
    {
        private List<Vase> _vases = new List<Vase>();

        private List<float> _insertSortedVases = new List<float>();
        private List<float> _mergeSortedVases = new List<float>();

        private string _selectionAlgoritmWorkTime;

        private int _selectionAlgoritmСomparisonsCount;
        private int _selectionAlgoritmExchangeCount;

        private string _mergeAlgoritmWorkTime;
        private int _mergeAlgoritmСomparisonsCount;
        private int _mergAlgoritmExchangeCount;

        public List<Vase> Vases
        {
            get => _vases;
            set => _vases = value;
        }

        public List<float> InsertSortedVases
        {
            get => _insertSortedVases;
            set => _insertSortedVases = value;
        }

        public List<float> MergeSortedVases
        {
            get => _mergeSortedVases;
            set => _mergeSortedVases = value;
        }

        public string SelectionAlgoritmWorkTime
        {
            get => _selectionAlgoritmWorkTime;
            set => _selectionAlgoritmWorkTime = value;
        }

        public string MergeAlgoritmWorkTime
        {
            get => _mergeAlgoritmWorkTime;
            set => _mergeAlgoritmWorkTime = value;
        }

        public int SelectionAlgoritmСomparisonsCount
        {
            get => _selectionAlgoritmСomparisonsCount;
            set => _selectionAlgoritmСomparisonsCount = value;
        }

        public int SelectionAlgoritmExchangeCount
        {
            get => _selectionAlgoritmExchangeCount;
            set => _selectionAlgoritmExchangeCount = value;
        }

        public int MergeAlgoritmСomparisonsCount
        {
            get => _mergeAlgoritmСomparisonsCount;
            set => _mergeAlgoritmСomparisonsCount = value;
        }

        public int MergeAlgoritmExchangeCount
        {
            get => _mergAlgoritmExchangeCount;
            set => _mergAlgoritmExchangeCount = value;
        }


    }
}