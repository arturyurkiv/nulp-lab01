﻿using System;
using System.Collections.Generic;
using System.Threading;
using Lab01.Controllers;
using Lab01.Models;
using Lab01.View;

namespace Application
{
    class Program
    {
      
        public static void Main(string[] args)
        {
            DataController.InitData();
            ResultView.GetSortingResult(DataController.DataResult);
        }

    }
}
